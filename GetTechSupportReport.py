import asyncio
import json
import logging
import pathlib
import re
import sys
import time

import aiohttp
from aiohttp import ClientSession

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.DEBUG,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("areq")
logging.getLogger("chardet.charsetprober").disabled = True


async def gen_tsr(
    ip: str,
    password: str,
    username: str,
    serial: str = "node",
    **kwargs,
) -> dict:
    """Push configuration changes from file to iDRAC and record results"""
    job = locals()

    auth = aiohttp.BasicAuth(login=username, password=password)

    async with ClientSession(auth=auth) as job["session"]:
        post_results = await export_tsr(**job)
        job.update(**post_results)
        if "job_id" in job:
            status_results = await parse_status(**job)
            job.update(**status_results)

    return job


async def parse_status(ip: str, job_id: str, session: ClientSession, start: int, **kwargs) -> dict:
    """Check status of configuration job by job_id."""
    job = dict()
    while True:
        response = await get_status(ip=ip, job_id=job_id, session=session)
        if response is None:
            await asyncio.sleep(1)
            continue

        data = await response.json()

        print(data)

        fail_messages = [
            "failed",
            "completed with errors",
            "Not one",
            "not compliant",
            "Unable",
            "The system could not be shut down",
            "No device configuration",
        ]

        success_messages = [
            "Successfully imported",
            "completed with errors",
            "Successfully imported",
        ]

        reboot_messages = [
            "No reboot Server",
        ]

        no_change_messages = [
            "No changes",
            "No configuration changes",
        ]

        if any(job_state(states=fail_messages, message=data["Oem"]["Dell"]["Message"])):
            logger.info(
                f"FAIL -- {job_id} marked as {data['Oem']['Dell']['JobState']} but detected issue(s). "
                f"See detailed job results below for more information on failure\n"
                f"Detailed job results for {job_id}\n"
                f"{data['Oem']['Dell']}\n"
                f"{data['Messages']}\n"
            )
            job["status"] = "failed"
            return job

        elif any(
            job_state(states=reboot_messages, message=data["Oem"]["Dell"]["Message"])
        ):
            logger.info(
                f"REBOOT -- job ID {job_id} successfully marked completed. NoReboot value detected and "
                f"config changes will not be applied until next manual server reboot\n"
                f"Detailed job results for {job_id}\n"
                f"{data['Oem']['Dell']}\n"
                f"{data['Messages']}\n"
            )
            job["status"] = "reboot needed"
            return job

        elif any(
            job_state(states=success_messages, message=data["Oem"]["Dell"]["Message"])
        ):
            end = time.perf_counter_ns()
            completion_time = (end - start) / 1e9

            logger.info(
                f"SUCCESS -- job ID {job_id} successfully marked completed\n"
                f"Detailed job results for job ID {job_id}\n"
                f"{data['Oem']['Dell']}\n"
                f"{job_id} completed in: {completion_time:.02f} seconds\n"
                f"Config results for job ID {job_id}\n"
                f"{data['Messages']}\n"
            )
            job["end"] = end
            job["completion_time"] = completion_time
            job["status"] = "completed"
            return job

        elif any(
            job_state(states=no_change_messages, message=data["Oem"]["Dell"]["Message"])
        ):
            logger.info(
                f"NO CHANGE -- job ID {job_id} marked completed\n"
                f"Detailed job results for job ID {job_id}\n"
            )
            job["status"] = "no change"
            return job

        else:
            logger.info(
                f"STATUS -- JobStatus not completed, current status: {data['Oem']['Dell']['Message']}, "
                f"percent complete: {data['Oem']['Dell']['PercentComplete']}"
            )
            await asyncio.sleep(30)
            continue


async def get_status(
    ip: str, session: ClientSession, job_id: str,
) -> aiohttp.ClientResponse:
    try:
        response = await session.get(
            url=f"https://{ip}/redfish/v1/TaskService/Tasks/{job_id}", ssl=False,
        )
        return response
    except (aiohttp.ClientError, aiohttp.ClientConnectionError,) as e:
        logger.error(
            f"aiohttp exception for {ip} [{getattr(e, 'status', None)}]: {getattr(e, 'message', None),}"
        )
    except Exception as e:
        logger.exception(
            f"Non-aiohttp exception occurred: {getattr(e, '__dict__', {})}"
        )


async def export_tsr(
    ip: str,
    serial: str,
    session: ClientSession,
    **kwargs: dict,
) -> dict:
    """POST request wrapper to push iDRAC configuration.
    """
    url = f"https://{ip}/redfish/v1/Dell/Managers/iDRAC.Embedded.1/DellLCService/Actions/DellLCService.ExportTechSupportReport"

    payload = {
        "IPAddress": "10.144.0.9",
        "ShareType": 'NFS',
        "ShareName": "/var/nfsshare",
        "DataSelectorArrayIn": [
            "HWData",
            "OSAppDataWithoutPII",
            "OSAppData",
            "TTYLogs",
        ]
    }

    headers = {"content-type": "application/json"}

    response = await session.post(
        url=url, data=json.dumps(payload), headers=headers, ssl=False,
    )
    response.raise_for_status()
    logger.info(f"Got response [{response.status}] for {serial}")

    try:
        job_id = re.search("JID_\d+", str(response)).group()
        if response.status != 202:
            logger.info(f"FAIL -- Got response [{response.status}] for {serial}")
        else:
            logger.info(f"SUCCESS -- {job_id} successfully created for {serial}")

        start = time.perf_counter_ns()

        job = {
            "job_id": job_id,
            "start": start,
        }

        return job
    except (aiohttp.ClientError, aiohttp.ClientConnectionError,) as e:
        logger.error(
            f"aiohttp exception for {serial} [{getattr(e, 'status', None)}]: {getattr(e, 'message', None),}"
        )
    except Exception as e:
        logger.exception(
            f"Non-aiohttp exception occurred: {getattr(e, '__dict__', {})}"
        )


def job_state(states: list, message: str) -> list:
    return [state for state in states if state in message]


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Python script using Redfish API to import the host server configuration profile locally from a "
        "configuration file."
    )
    parser.add_argument("-ip", help="iDRAC IP address", required=True)
    parser.add_argument("-u", "--username", help="iDRAC username", required=True)
    parser.add_argument("-p", "--password", help="iDRAC password", required=True)

    args = vars(parser.parse_args())

    asyncio.run(gen_tsr(**args))
