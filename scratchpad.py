mounts = []

for i in range(1, 25):
    drives = {
        "hdfs": f"/data{i:02}/hdfs",
        "kudu": f"/data{i:02}/kudu{i:02}"
    }

    mounts.append(drives)

print(mounts)
