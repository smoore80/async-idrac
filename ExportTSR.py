import asyncio
import csv
import pathlib
import typing

import pandas as pd

from tsm_parser import parse_data
from GetTechSupportReport import gen_tsr

here = pathlib.Path(__file__).parent

input_path = here.joinpath("hadoop_nodes.csv")

output_path = here.joinpath("status.csv")


def find_serial_in_list_of_lists(input_list, element):
    for sub_list in input_list:
        if element in sub_list:
            return input_list.index(sub_list), sub_list.index(element)


def find_nodes(input_path: pathlib.Path):
    nodes = list()
    leases = pathlib.Path("/var/lib/misc/dnsmasq.leases")
    with leases.open('r') as fin:
        leases_in = fin.read()

    leases_parse = parse_data("./templates/textfsm/leases.textfsm", leases_in)

    with input_path.open("r") as fin:
        reader = csv.DictReader(fin)
        for row in reader:
            match = find_serial_in_list_of_lists(leases_parse, row['serial'])
            if match:
                values = {
                    'serial': leases_parse[match[0]][match[1]],
                    'ip': leases_parse[match[0]][1],
                    'new_ip': row['oob_ip'],
                    'subnet_mask': row['oob_subnet'],
                    'gateway': row['oob_gateway'],
                    'filename': f"templates/configs/{row['type']}-post.json",
                    'password': 'calvin',
                    'username': 'root',
                    'target': 'ALL',
                }
                nodes.append(values)

    return nodes


def create_status_report(data: list, output_path: pathlib.Path) -> None:
    df = pd.DataFrame(data)
    with output_path.open("w") as fout:
        df.to_csv(fout)


print('Staring iDRAC update')

nodes = find_nodes(input_path=input_path)
tasks = [gen_tsr(**node) for node in nodes]
responses = asyncio.gather(*tasks)

loop = asyncio.get_event_loop()
results = loop.run_until_complete(responses)

create_status_report(data=results, output_path=output_path)

print('Completed!')
