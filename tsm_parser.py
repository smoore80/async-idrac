import textfsm


def parse_data(template_file, parse_input):
    """
    Parse data returned from commands ran on remote device into json formatted data.
    :param template_file: String, name of template file to use to parse data.
    :param parse_input: Variable containing data to be parsed.
    :return: json formatted data.
    """
    template = open(template_file)
    parser = textfsm.TextFSM(template)
    fsm_results = parser.ParseText(parse_input)
    return fsm_results
