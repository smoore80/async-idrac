import asyncio
import csv
import json
import logging
import pathlib
import sys

import aiohttp
from aiohttp import ClientSession

import pandas as pd

from tsm_parser import parse_data

logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.DEBUG,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("areq")
logging.getLogger("chardet.charsetprober").disabled = True


async def deploy_net(
    ip: str,
    password: str,
    username: str,
    static_ip: str,
    static_gateway: str,
    static_subnetmask: str,
    hostname: str,
    domain_name: str,
    serial: str = "node",
    **kwargs,
) -> dict:
    """Push network configuration changes to iDRAC and record results"""
    job = locals()

    auth = aiohttp.BasicAuth(login=username, password=password)

    async with ClientSession(auth=auth) as job["session"]:
        clear_logs_results = await clear_logs(**job)
        job["clear_logs"] = clear_logs_results.status

        patch_hostname_results = await patch_hostname(**job)
        job["patch_hostname"] = await patch_hostname_results.json()

        patch_attributes_results = await patch_attributes(**job)
        job["patch_attributes"] = await patch_attributes_results.json()

    return job


async def clear_logs(
    ip: str, serial: str, session: ClientSession, **kwargs
) -> aiohttp.ClientResponse:
    url = f"https://{ip}/redfish/v1/Managers/iDRAC.Embedded.1/LogServices/Sel/Actions/LogService.ClearLog"
    headers = {"content-type": "application/json"}

    try:
        response = await session.post(url=url, headers=headers, ssl=False,)
        response.raise_for_status()
        logger.info(f"Got response [{response.status}] for {serial}")

        return response

    except (aiohttp.ClientError, aiohttp.ClientConnectionError,) as e:
        logger.error(
            f"aiohttp exception for {serial} [{getattr(e, 'status', None)}]: {getattr(e, 'message', None),}"
        )

        return e

    except Exception as e:
        logger.exception(
            f"Non-aiohttp exception occurred: {getattr(e, '__dict__', {})}"
        )

        return e


async def patch_hostname(
    ip: str, hostname: str, serial: str, session: ClientSession, **kwargs
) -> aiohttp.ClientResponse:
    url = f"https://{ip}/redfish/v1/Managers/iDRAC.Embedded.1/EthernetInterfaces/iDRAC.Embedded.1%23NIC.1"
    payload = {
        "HostName": hostname,
    }
    headers = {"content-type": "application/json"}

    try:
        response = await session.patch(
            url=url, data=json.dumps(payload), headers=headers, ssl=False,
        )
        response.raise_for_status()
        logger.info(f"Got response [{response.status}] for {serial}")

        return response

    except (aiohttp.ClientError, aiohttp.ClientConnectionError,) as e:
        logger.error(
            f"aiohttp exception for {serial} [{getattr(e, 'status', None)}]: {getattr(e, 'message', None),}"
        )

        return e

    except Exception as e:
        logger.exception(
            f"Non-aiohttp exception occurred: {getattr(e, '__dict__', {})}"
        )

        return e


async def patch_attributes(
    ip: str,
    serial: str,
    static_ip: str,
    static_gateway: str,
    static_subnetmask: str,
    domain_name: str,
    session: ClientSession,
    **kwargs,
) -> aiohttp.ClientResponse:
    url = f"https://{ip}/redfish/v1/Managers/iDRAC.Embedded.1/Attributes/"
    payload = {
        "Attributes": {
            "NICStatic.1.DNSDomainName": domain_name,
            "IPv4.1.Address": static_ip,
            "IPv4.1.DHCPEnable": "Disabled",
            "IPv4.1.Enable": "Enabled",
            "IPv4.1.Gateway": static_gateway,
            "IPv4.1.Netmask": static_subnetmask,
            "IPv4Static.1.Address": static_ip,
            "IPv4Static.1.Gateway": static_gateway,
            "IPv4Static.1.Netmask": static_subnetmask,
        }
    }
    headers = {"content-type": "application/json"}

    try:
        response = await session.patch(
            url=url, data=json.dumps(payload), headers=headers, ssl=False,
        )
        response.raise_for_status()
        logger.info(f"Got response [{response.status}] for {serial}")

        return response

    except (aiohttp.ClientError, aiohttp.ClientConnectionError,) as e:
        logger.error(
            f"aiohttp exception for {serial} [{getattr(e, 'status', None)}]: {getattr(e, 'message', None),}"
        )

        return e

    except Exception as e:
        logger.exception(
            f"Non-aiohttp exception occurred: {getattr(e, '__dict__', {})}"
        )

        return e


def find_serial_in_list_of_lists(input_list, element):
    for sub_list in input_list:
        if element in sub_list:
            return input_list.index(sub_list), sub_list.index(element)


def find_nodes(input_path: pathlib.Path):
    nodes = list()
    leases = pathlib.Path("/var/lib/misc/dnsmasq.leases")
    with leases.open("r") as fin:
        leases_in = fin.read()

    leases_parse = parse_data("./templates/textfsm/leases.textfsm", leases_in)

    with input_path.open("r") as fin:
        reader = csv.DictReader(fin)
        for row in reader:
            match = find_serial_in_list_of_lists(leases_parse, row["serial"])
            if match:
                values = {
                    "serial": leases_parse[match[0]][match[1]],
                    "ip": leases_parse[match[0]][1],
                    "static_ip": row["oob_ip"],
                    "static_subnetmask": row["oob_subnet"],
                    "static_gateway": row["oob_gateway"],
                    "hostname": f"{row['hostname']}-rm",
                    "password": "calvin",
                    "username": "root",
                    "domain_name": "prod.pncint.net",
                }
                nodes.append(values)

    return nodes


def create_status_report(data: list, output_path: pathlib.Path) -> None:
    df = pd.DataFrame(data)
    with output_path.open("w") as fout:
        df.to_csv(fout)


print("Staring iDRAC update")

here = pathlib.Path(__file__).parent

input_path = here.joinpath("hadoop_nodes.csv")

output_path = here.joinpath("idrac_status.csv")

nodes = find_nodes(input_path=input_path)
tasks = [deploy_net(**node) for node in nodes]
responses = asyncio.gather(*tasks)

loop = asyncio.get_event_loop()
results = loop.run_until_complete(responses)

create_status_report(data=results, output_path=output_path)

print("Completed!")
