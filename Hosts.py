import csv
import pathlib

from tsm_parser import parse_data


def format_mac(mac_string, sep):
    return f'{sep}'.join(mac_string[i:i+2] for i in range(0, 12, 2))


def find_serial_in_list_of_lists(input_list, element):
    for sub_list in input_list:
        if element in sub_list:
            return input_list.index(sub_list), sub_list.index(element)


def find_nodes(filename: str, leases: str) -> list:
    nodes = list()
    with open(leases, 'r') as lin:
        leases_in = lin.read()

    leases_parse = parse_data("templates/textfsm/leases.textfsm", leases_in)

    with open(filename, "r") as fin:
        reader = csv.DictReader(fin)
        for row in reader:
            mac = format_mac(row['nic_mac'], ':').lower()
            match = find_serial_in_list_of_lists(leases_parse, mac)
            if match:
                values = {
                    'serial': row['serial'],
                    'hostname': f"{row['hostname']}.prod.pncint.net",
                    'rack': f"{row['location']}_{row['rack']}",
                    'current_ip_addr': leases_parse[match[0]][1],
                    'new_ip_addr': row['prod_ip'],
                    'subnet_mask': row['prod_subnet'],
                    'gateway': row['prod_gateway'],
                    'mac': row['nic_mac'],
                    'lease_mac': mac,
                    'type': row['type'],
                }
                nodes.append(values)

    return nodes


def create_output_file(hosts: list) -> None:
    with open("hosts_output", "w") as fout:
        for host in hosts:
            line = f"[{host['rack']}_{host['type']}]\n{host['hostname']} ansible_host={host['current_ip_addr']}\n"
            fout.write(line)


def create_host_vars(hosts: list) -> None:
    template = """network:
  DEVICE: em1
  ONBOOT: yes
  BOOTPROTO: none
  IPADDR: {ip}
  PREFIX: {prefix}
  GATEWAY: {gateway}
"""

    for host in hosts:
        with open(f"host_vars/{host['hostname']}", "w") as fout:
            fout.write(template.format(
                ip=host['new_ip_addr'],
                prefix=24,
                gateway=host['gateway']
            ))


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description=f"Python script to pull MAC to IP mappings from dnsmaq.leases."
    )
    parser.add_argument(
        "-f",
        "--filename",
        help="Filename of .csv file contains MAC addresses\n"
             "Format as follows:\n"
             "location,rack,grid,ru,type,hostname,serial,oob_mac,oob_ip,oob_gateway,oob_subnet,"
             "nic_mac,prod_ip,prod_gateway,prod_subnet",
        required=True
    )
    parser.add_argument(
        "-l",
        "--leases",
        default="/var/lib/misc/dnsmasq.leases",
        help="Path to leases file if not /var/lib/misc/dnsmasq.leases",
        required=False
    )
    args = vars(parser.parse_args())

    print(args)
    nodes = find_nodes(**args)
    create_output_file(nodes)
    create_host_vars(nodes)
