from setuptools import setup

setup(
    name="async-idrac",
    version="19.11.01",
    url="https://github.com/Spatcholla/async-idrac",
    license="MIT License",
    author="samuel",
    author_email="Samuel Moore",
    description="Scripts to interacting with the Redfish API and iDRAC.",
    packages=["aiohttp", "pandas", "textfsm"],
)
