import asyncio
import json
import logging
import sys

import aiohttp
from aiohttp import ClientSession


logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.DEBUG,
    datefmt="%H:%M:%S",
    stream=sys.stderr,
)
logger = logging.getLogger("areq")
logging.getLogger("chardet.charsetprober").disabled = True

nodes = ["10.144.2.127"]
# end_point = {
#     "tsr": "/redfish/v1/Dell/Managers/iDRAC.Embedded.1/DellLCService/Actions/DellLCService.ExportTechSupportReport",
#     "clear_log": "/redfish/v1/Managers/iDRAC.Embedded.1/LogServices/Sel/Actions/LogService.ClearLog"
# }


async def deploy_net(
    ip: str,
    password: str = "calvin",
    username: str = "root",
    static_ip: str = "foo",
    static_gateway: str = "bar",
    static_subnetmask: str = "baz",
    hostname: str = "foobar",
    domain_name: str = "foobaz",
    serial: str = "node",
    **kwargs,
) -> dict:
    """Push network configuration changes to iDRAC and record results"""
    job = locals()

    auth = aiohttp.BasicAuth(login=username, password=password)

    async with ClientSession(auth=auth) as job["session"]:
        clear_logs_results = await clear_logs(**job)
        logger.info(f"{clear_logs_results}")
        clear_logs_status = clear_logs_results.status
        logger.info(f"{clear_logs_status}")
        job["clear_logs"] = clear_logs_status

        # patch_hostname_results = await patch_hostname(**job)
        # job["patch_hostname"] = await patch_hostname_results.json()
        #
        # patch_attributes_results = await patch_attributes(**job)
        # job["patch_attributes"] = await patch_attributes_results.json()

    return job


async def clear_logs(
    ip: str, serial: str, session: ClientSession, **kwargs
) -> aiohttp.ClientResponse:
    url = f"https://{ip}/redfish/v1/Managers/iDRAC.Embedded.1/LogServices/Sel/Actions/LogService.ClearLog"
    headers = {"content-type": "application/json"}

    try:
        response = await session.post(url=url, headers=headers, ssl=False,)
        response.raise_for_status()
        logger.info(f"Got response [{response.status}] for {serial}")

        return response

    except (aiohttp.ClientError, aiohttp.ClientConnectionError,) as e:
        logger.error(
            f"aiohttp exception for {serial} [{getattr(e, 'status', None)}]: {getattr(e, 'message', None),}"
        )

        return e

    except Exception as e:
        logger.exception(
            f"Non-aiohttp exception occurred: {getattr(e, '__dict__', {})}"
        )

        return e

# async def post_config(node, destination):
#     url = f"https://{node}{destination}"
#     payload = {
#         "IPAddress": "10.144.0.9",
#         "ShareType": 'NFS',
#         "ShareName": "/var/nfsshare",
#         "DataSelectorArrayIn": [
#             "HWData",
#             "OSAppDataWithoutPII",
#             "OSAppData",
#             "TTYLogs",
#         ]
#     }
#
#     auth = aiohttp.BasicAuth(login="root", password="calvin")
#
#     async with ClientSession(auth=auth) as session:
#         headers = {"content-type": "application/json"}
#
#         response = await session.post(
#             url=url, data=json.dumps(payload), headers=headers, ssl=False,
#         )
#
#     return response


tasks = [deploy_net(ip=node) for node in nodes]
responses = asyncio.gather(*tasks)

loop = asyncio.get_event_loop()
results = loop.run_until_complete(responses)
print(results)
